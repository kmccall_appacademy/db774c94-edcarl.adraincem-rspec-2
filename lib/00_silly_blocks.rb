def reverser
  str = yield.split
  str.each_with_index { |word, idx| str[idx] = word.reverse }
  str.join(' ')
end

def adder(num = 1)
  num + yield
end

def repeater(n = 0, &block)
  if n == 0
    block.call
  else
    n.times { block.call }
  end
end
