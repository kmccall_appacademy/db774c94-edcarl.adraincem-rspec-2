def measure(num = 0, &prc)
  start_time = Time.now

  if num == 0
    prc.call
  else
    num.times { prc.call }
  end

  time_elapsed = Time.now - start_time
  return time_elapsed if num == 0
  time_elapsed / num
end
